﻿package com.cumulitech.gallery;

/**
 * GridView的每个item的数据对象
 * 
 * @author 
 *
 */
public class ImagesBean{
	/**
	 * 图片路径
	 */
	private String ImagePath;
	/**
	 * 文件名
	 */
	private String fileName; 
	
	public String getImagePath() {
		return ImagePath;
	}
	public void setImagePath(String ImagePath) {
		this.ImagePath = ImagePath;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}	
}
